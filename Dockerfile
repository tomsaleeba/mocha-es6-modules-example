FROM node:14-alpine
RUN npm install -g pnpm
WORKDIR /app
RUN chown node:node .
USER node
ADD package.json pnpm-lock.yaml ./
RUN pnpm i
ADD src/ ./src
ADD test/ ./test
ENTRYPOINT [ "/usr/local/bin/pnpm", "test" ]
