> An MVP to show what is needed to be able to test ES6 modules with `mocha`

# Important points
- you need a new enough version of node: v13.2.0 or newer. This version is when
  ES module support no longer required a feature flag. See [notes for NodeJS
  support from
  MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules#import)
- your `package.json` must have
    ```json
    "type": "module",
    ```
- presumably you need a new enough version of `mocha`. Not sure what that is
  but the version in this project works.

# How to run
1. clone repo
1. install dependencies
    ```bash
    pnpm i
    ```
1. run tests
    ```bash
    pnpm test
    ```

# Running with docker
This requires that you have docker installed.
```bash
# clone this repo
cd mocha-es6-modules-example/
npm run docker:build
npm run docker:run
```

# Why `pnpm` and not `yarn`
`pnpm` works great and doesn't let Facebook be a middleman for the whole JS
ecosystem.
