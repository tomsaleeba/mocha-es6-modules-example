import assert from 'assert'
import * as objectUnderTest from '../src/apple.mjs'

describe('apple', () => {
  describe('getShape', () => {
    it('should be like a sphere', () => {
      const result = objectUnderTest.getShape()
      assert.equal(result.endsWith('spherical'), true)
    })
  })
})
