import assert from 'assert'
import * as objectUnderTest from '../src/banana.mjs'

describe('banana', () => {
  describe('getColour', () => {
    it('should be yellow', () => {
      const result = objectUnderTest.getColour()
      assert.equal(result, 'yellow')
    })
  })
})
